﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWallet_WalletProject
{
    class Wallet
    {
        private double wCash;
        private int wCards;
        private int wIds;

        public Wallet(double _cash, int _cards, int _Ids)
        {
            wCash = _cash;
            wCards = _cards;
            wIds = _Ids;
        }
        
        public double Cash
        {
            get
            {
                return wCash;
            }
            set
            {
                wCash = value;
            }
        }

        public int Cards
        {
            get
            {
                return wCards;
            }
            set
            {
                wCards = value;
            }
        }

        public int Ids
        {
            get
            {
                return wIds;
            }
            set
            {
                wIds = value;
            }
        }
        
    }
}
