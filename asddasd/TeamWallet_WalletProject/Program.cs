﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWallet_WalletProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Blake Hughes, Trae Wright, Jair Milton
            //DVP 1705

            //ask user for wallet info, get ID's (max 5), cards(max 5), money
            //"Enter the amount of change you have in bills: 25"
            //"Enter the amount of change you have in coins: 135"
            //"Your wallet balance is 26 dollars and 35 cents. Is this correct? Y or N?"
            //Enter your card:"black amex charge card"
            Console.Clear();
            Console.Write("Enter the amount of money you have in your wallet, example(12.56): ");
            double cash = Validator.DoubleCheck();

            //cards

            Console.Write("Enter the number of card you have in your wallet(max of 5 cards): ");
            int cards = Validator.IntCheck();
            while (cards > 5)
            {
                Console.Write("Enter the number of card you have in your wallet(MAX OF 5 CARDS): ");
                cards = Validator.IntCheck();
            }
            string[] cardNames = new string[5];
            for (int i = 0; i <= cards - 1; i++)
            {
                Console.Clear();
                Console.WriteLine("Please enter in a name for card number {0}.", i + 1);
                cardNames[i] = Validator.StringCheck();
                
            }
            if (cards != 0)
            {
                cardNames = CardsIds.cardChange(cardNames,cards);//This is to change any cards

            }//If statement. end of cards



            Console.Clear();
                //Ids

                Console.Write("Enter the number of ids you have in your wallet(max of 3): ");
            int ids = Validator.IntCheck();
            while (ids > 3)
            {
                Console.Write("Enter the number of ids you have in your wallet(MAX OF 3 CARDS): ");
                ids = Validator.IntCheck();
            }
            string[] idNames = new string[3];

            for (int i = 0; i <= ids - 1; i++)
            {
                Console.Clear();
                Console.WriteLine("Please enter in a name for id number {0}.", i + 1);
                idNames[i] = Validator.StringCheck();

            }
            if (ids != 0)
            {
                idNames = CardsIds.idChange(idNames,ids);//This is to change any ids
            }// end of ids

            
            Boolean changes = false;

            while (!changes)
            {
                Console.Clear();
                Console.WriteLine("The total amount of cash you have is ${0}.",cash);
                CardsIds.cardReadOut(cardNames, cards);
                CardsIds.idReadOut(idNames, ids);
                Console.WriteLine("\r\n\r\nWhat would you like to do next?\r\nType 1 to change your cash\r\nType 2 to edit your cards\r\nType 3 to edit your ids\r\nType 4 to exit\r\n\r\nPress enter when ready.");
                int task = Validator.IntCheck();
                while (!(task > 0 || task < 5))
                {
                    Console.WriteLine("Invalid input. Please only enter a number from 1 to 4.");
                    task = Validator.IntCheck();
                }
                Console.Clear();

                if (task == 1)
                {
                    Console.WriteLine("Your current balance is ${0}. Do you want to add cash to your wallet or do you want to use it?", cash);

                    string addoruseString = Console.ReadLine();
                    while (!(addoruseString.ToLower() == "add" || addoruseString.ToLower() == "use"))
                    {
                        Console.WriteLine("Invalid Response. Please only type in Add or Use to start the program.");
                        addoruseString = Console.ReadLine();
                    }
                    Console.Clear();
                    if (addoruseString.ToLower() == "add")
                    {
                        Console.WriteLine("Choose the amount you wish to add?");
                        double addedNum = Validator.DoubleCheck();
                        cash = cash + addedNum;
                        Console.WriteLine(" You now have a total of $ {0} .", cash);

                    }
                    else if (addoruseString.ToLower() == "use")
                    {
                        Console.WriteLine("Choose the amount of cash you wish to use?");
                        double subNum = Validator.DoubleCheck();
                        cash = cash - subNum;
                        Console.WriteLine("You now have a total of $ {0}", cash);
                    }
                    else
                    {
                        Console.WriteLine("Please type in Add or Use to start the program.");
                    }
                } else if (task == 2)
                {
                    Console.WriteLine("Would you like to add, view, or edit a card?");
                    string cardAction = Console.ReadLine();
                    while (!(cardAction.ToLower() == "add" || cardAction.ToLower() == "edit" || cardAction.ToLower() == "view"))
                    {
                        Console.WriteLine("Invalid Response. Please only type in Add, view, or edit.");
                        cardAction = Console.ReadLine();
                    }
                    Console.Clear();
                    if (cardAction.ToLower() == "add")
                    {
                        if (cards != 5)
                        {
                            Console.WriteLine("Please enter the name for the new card.");
                            cardNames[cards] = Validator.StringCheck();
                            cards = cards + 1;
                        }
                        else
                        {
                            Console.WriteLine("Sorry, you already have the max amount of cards");
                        }

                    }
                    else if (cardAction.ToLower() == "edit")
                    {
                        cardNames = CardsIds.cardChange(cardNames,cards);
                    }else
                    {
                        CardsIds.cardReadOut(cardNames, cards);
                    }

                } else if (task == 3)
                {
                    Console.WriteLine("Would you like to add, view, or edit an id?");
                    string idAction = Console.ReadLine();
                    while (!(idAction.ToLower() == "add" || idAction.ToLower() == "edit" || idAction.ToLower() == "view"))
                    {
                        Console.WriteLine("Invalid Response. Please only type in Add, view, or edit.");
                        idAction = Console.ReadLine();
                    }
                    Console.Clear();
                    if (idAction.ToLower() == "add")
                    {
                        if (ids != 3)
                        {
                        Console.WriteLine("Please enter the name for the new id.");
                        idNames[ids] = Validator.StringCheck();
                            ids = ids + 1;

                        }
                        else
                        {
                            Console.WriteLine("Sorry but you already have the maximum amount of ids.");
                        }
                       
                    }
                    else if (idAction.ToLower() == "edit")
                    {
                        idNames = CardsIds.idChange(idNames,ids);
                    }
                    else
                    {
                        CardsIds.idReadOut(idNames, ids);

                    }
                } else
                {
                    Console.WriteLine("Thank you, your wallet is ready to go have a nice day.");
                    changes = true;
                }

            }


        }//
    }
}
