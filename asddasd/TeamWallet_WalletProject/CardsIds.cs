﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWallet_WalletProject
{
    class CardsIds
    {
        public static string[] idChange(string [] _idNames,int _id)
        {
            string idCheck = "no";
            while (idCheck.ToLower() == "no")
            {
                idReadOut(_idNames,_id);
                Console.WriteLine("\r\nAre all of the names right?(yes or no)");
                idCheck = Console.ReadLine();
                while (!(idCheck.ToLower() == "yes" || idCheck.ToLower() == "no"))
                {
                    Console.WriteLine("\r\nInvalid input.\r\nAre all of the names right?\r\nPlease only enter \"yes\" or \"no\".");
                    idCheck = Console.ReadLine();
                }
                if (idCheck.ToLower() == "no")
                {
                    Console.WriteLine("What id number is wrong?");
                    String wrongIdSt = Console.ReadLine();
                    int wrongId;
                    while (!(int.TryParse(wrongIdSt, out wrongId)) || (wrongId > _idNames.Length && wrongId < 1))
                    {
                        Console.WriteLine("Invalid input.\r\nWhat id number is wrong? Please only enter in a number from 1 to {0}.", _idNames.Length);
                        wrongIdSt = Console.ReadLine();
                    }
                    Console.WriteLine("Ok, please enter in the correct name for id number {0}.", wrongId);
                    _idNames[wrongId - 1] = Validator.StringCheck();
                }
            }//WHILE LOOP
            return _idNames;
        }


        public static void idReadOut(string[] _idNames, int _id)
        {
            for (int i = 0; i <= _id-1; i++)
            {
                Console.WriteLine("The name you entered for id number {0} is {1}.", i + 1, _idNames[i]);

            }
        }


        public static string[] cardChange(string[] _cardNames,int _cards)
        {
            string nameCheck = "no";
            while (nameCheck.ToLower() == "no")
            {
                cardReadOut(_cardNames,_cards);
                Console.WriteLine("\r\nAre all of the names right?(yes or no)");
                nameCheck = Console.ReadLine();

                while (!(nameCheck.ToLower() == "yes" || nameCheck.ToLower() == "no"))
                {
                    Console.WriteLine("\r\nInvalid input.\r\nAre all of the names right?\r\nPlease only enter \"yes\" or \"no\".");
                    nameCheck = Console.ReadLine();
                }
                if (nameCheck.ToLower() == "no")
                {
                    Console.WriteLine("What card number is wrong?");
                    String wrongCardSt = Console.ReadLine();
                    int wrongCard;
                    while (!(int.TryParse(wrongCardSt, out wrongCard)) || (wrongCard > _cardNames.Length && wrongCard < 1))
                    {
                        Console.WriteLine("Invalid input.\r\nWhat card number is wrong? Please only enter in a number from 1 to {0}.", _cardNames.Length);
                        wrongCardSt = Console.ReadLine();
                    }
                    Console.WriteLine("Ok, please enter in the correct name for card number {0}.", wrongCard);
                    _cardNames[wrongCard - 1] = Validator.StringCheck();
                }
            }
            return _cardNames;
        }

        public static void cardReadOut(string[] _cardNames,int _cards)
        {
            for (int i = 0; i <= _cards - 1; i++)
            {
                Console.WriteLine("The name you entered for card number {0} is {1}.", i + 1, _cardNames[i]);

            }
        }


    }
}
